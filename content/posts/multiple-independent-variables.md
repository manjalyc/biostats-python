---
title: "Multiple Independent Variables"
date: 2023-04-04
draft: false
author: "Cyriac Manjaly"
UseHugoToc: false
tags: ["multiple sample tests", "anova", "manova", "n-way anova", "Code"]
showToc: true
weight: 40
cover:
    hidden: false
    relative: false
math: true
---

# Multiple Independent Variables

Up to this point, we have been considering tests for 1 independent variable. What if we want to test for multiple independent variables? In this section we will explore N-way ANOVA, MANOVA, 

## Examples

---
### Ex. 1: 1-way ANOVA

What if we want to compare the average length of songs in three different albums we could conduct a 1-way ANOVA.

**Code**
```python
from scipy.stats import f_oneway

a1_lengths = [103, 83, 89, 97, 124, 187, 98, 158, 128]
a2_lengths = [232, 243, 120, 188, 198, 183, 167, 170, 31]
a3_lengths = [133, 43, 97, 122, 127, 48, 158, 128]

print(f_oneway(a1_lengths, a2_lengths, a3_lengths))
```

**Output**

`F_onewayResult(statistic=4.2128474386129975, pvalue=0.02761174698879269)`

At a significance level of 5%, given our p-value of 0.028, we would find that our album song lengths significantly differed!

---

### Ex. 2: Kruskal Wallis

The Kruskal Wallis Test is a nonparametric alternative to a 1-way ANOVA. It is based upon ranks, and used when normality cannot be assumed.

**Code**

```python
from scipy.stats import kruskal

a1_lengths = [103, 83, 89, 97, 1240, 187, 98, 158, 1280]
a2_lengths = [2320, 243, 120, 1880, 1980, 183, 167, 170, 31]
a3_lengths = [1330, 43, 97, 1220, 127, 48, 158, 1280]

print(kruskal(a1_lengths, a2_lengths, a3_lengths))
```

**Output**

`KruskalResult(statistic=2.2469032816183767, pvalue=0.3251555353544761))`

At a significance level of 5%, given our p-value of 0.325, we would not find that our album song lengths significantly differed.

#### Sources

- https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.f_oneway.html
- https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.kruskal.html
- [Benevolent Stats](https://www.amazon.sg/Benevolent-Stats-MATLAB-Statistical-Analysis/dp/B08SGWNKZJ)