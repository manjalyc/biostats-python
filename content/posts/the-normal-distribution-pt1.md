---
title: "The Normal Distribution (Pt. 1)"
date: 2023-03-28
draft: false
author: "Cyriac Manjaly"
UseHugoToc: false
tags: ["Normal Distribution", "Gaussian Distribution", "z-score", "Code"]
showToc: true
weight: 12
cover:
    hidden: false
    relative: false
math: true
---

# The Normal Distribution (Pt. 1)

The normal distribution is a continuous probability distribution that is symmetrical around its mean and designed for independent, random variables.

In this first section, we analyze how to calculate the CDF and z-score when provided a normal distribution. In the next section, we will analyze how to work with data

## Working with The Normal Distribution

Throughout these examples we will consider student's test scores whose distribution we know to have an average of 80, and a standard deviation of 10.

Our first two examples will work with the normal cumulative distribution function (CDF).

Our last example will calculate a z-score using the 

### Ex. 1: Probability of scoring below a 70

Let's say we have a bunch of test scores with an average of 80 and a standard deviation of 10. If we want to calculate how many students scored below a 70 we can do the following:

norm.cdf(70, loc = mean, scale = std_dev)

```Python
from statistics import NormalDist

mean = 80
std_dev = 10
dist = NormalDist(mu=mean, sigma=std_dev)

print("The probability a student scored below a 70 is",  dist.cdf(70))
```

Output

`The probability a student scored below a 70 is 0.15865525393145713`

So there is a 16% chance that any individual student scored below a 70.

### Ex. 2: Probability of scoring above an 85

Let's say we have a bunch of test scores with an average of 80 and a standard deviation of 10. If we want to calculate how many students scored above an 85 we can simply do 1 - P(Student Score Below an 85):

```Python
from statistics import NormalDist

mean = 80
std_dev = 10
dist = NormalDist(mu=mean, sigma=std_dev)

print("The probability a student scored above an 85 is", 1 - dist(85))
```

Output

`The probability a student scored above an 85 is 0.308537538725987`

So there is a 31% chance that any individual student scored above an 85.


### Ex. 3: Calculating a Z-Score

Lets say we want to calculate the z-score of a student that scored at 50.


```Python
mean = 80
std_dev = 10
student_score = 50

print("The z-score of a student who scored a", student_score, "is", 
      (float(student_score) - mean ) / std_dev)
```

Output

`The z-score of a student who scored a 50 is -3.0`

#### Sources
- https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.norm.html
- https://statisticsbyjim.com/basics/normal-distribution/
- https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.anderson.html
- [Benevolent Stats](https://www.amazon.sg/Benevolent-Stats-MATLAB-Statistical-Analysis/dp/B08SGWNKZJ)