---
title: "1-sample Tests"
date: 2023-04-02
draft: false
author: "Cyriac Manjaly"
UseHugoToc: false
tags: ["1-sample tests", "1-sample sign test for medians", "1-sample t-test", "Code"]
showToc: true
weight: 14
cover:
    hidden: false
    relative: false
math: true
---

# 1-Sample Tests

One-sample tests are used to compare a sample to a population. Here we provide examples for conducting a one-sample t-test and one-sample sign-rank test. 

## Examples

---
### Ex. 1: One-sample t-test

Lets say that the average song lengths of all songs ever made is 147 seconds, but we don't know the standard deviation. If we were to test if the song lengths in an album differed from all the songs in the world, we could conduct a one sample t-test:

**Code**
```python
from scipy.stats import ttest_1samp 

pop_avg_song_length = 147
album_song_lengths = [103, 83, 89, 97, 124, 187, 98, 158, 128]

print(ttest_1samp(album_song_lengths, pop_avg_song_length))
```

**Output**

`TtestResult(statistic=-2.4600037123802294, pvalue=0.039318532181634856, df=8)`

At a significance level of 5%, given our p-value of 0.039, we would find that our album song lengths significantly differed from the population!

---

### Ex. 2: One-Sample Sign Test for Medians

What if our data is non-parametric (not normal), or we have a very small sample size, and huge outliers? We could instead do an one-sample sign test for medians.

**Code**

```python
from statsmodels.stats.descriptivestats import sign_test 

pop_avg_song_length = 147 # We will treat this as a median
album_song_lengths = [4020, 103, 81, 162, 64, 42]

print("(M, p-value):", 
      sign_test(album_song_lengths, mu0=pop_avg_song_length))
```

**Output**

`(M, p-value): (-1.0, 0.6875)`

At a significance level of 5%, our p-value of 0.6875 indicates that our album song lengths are not significantly different from the median length of a pop-song.

#### Sources

- https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.ttest_1samp.html
- https://www.statsmodels.org/dev/generated/statsmodels.stats.descriptivestats.sign_test.html
- [Benevolent Stats](https://www.amazon.sg/Benevolent-Stats-MATLAB-Statistical-Analysis/dp/B08SGWNKZJ)