---
title: "Mixed Effects Model"
date: 2023-04-18
draft: false
author: "Cyriac Manjaly"
UseHugoToc: false
tags: ["multiple sample tests", "mixed effects model", "Code"]
showToc: true
weight: 55
cover:
    hidden: false
    relative: false
math: true
---

# The Mixed Effects Model

Mixed Effects models are similar to regression models in that you are trying to predict a single dependent variable based upon independent variables of any type.

Regular regression models always have errors in the form of residuals, mixed effects try to give structure to those residuals by allowing entities sampled multiple times to have different baselines.

*Anytime you have multiple collections from the same entity/person/subject, a Mixed Effects model should be considered.*

## Examples

---

For our examples we will work pig data. While the table is too big to put on display here, a few rows are displayed below.

The csv data can be downloaded from [github](https://github.com/vincentarelbundock/Rdatasets/blob/master/csv/geepack/dietox.csv) or [R-DATA](https://r-data.pmagunia.com/dataset/r-dataset-package-geepack-dietox)

Here's a [direct download link](https://r-data.pmagunia.com/system/files/datasets/dataset-30685.csv) and [another direct download link](https://raw.githubusercontent.com/vincentarelbundock/Rdatasets/master/csv/geepack/dietox.csv)


**Data in *'pigdata.csv'***

*First Few Rows of the Table*

|Weight|Feed|Time|Pig |Evit|Cu |Litter|
|------|----|----|----|----|---|------|
|26.5  |NA  |1   |4601|1   |1  |1     |
|27.59999|5.200005|2   |4601|1   |1  |1     |
|36.5  |17.6|3   |4601|1   |1  |1     |
|40.29999|28.5|4   |4601|1   |1  |1     |
|49.09998|45.200001|5   |4601|1   |1  |1     |
|55.39999|56.900002|6   |4601|1   |1  |1     |
|59.59998|71.700005|7   |4601|1   |1  |1     |
|...   |... |...  |...  |... |... |...   |


---

### Ex. 1: Random Intercept Model

In a random intercept model we assume that the baseline for each subject is different. An appropriate model for this data might be to consider how weight is a function of time with a different baseline for each pig.

**Code**

```python
import pandas as pd
import statsmodels.api as sm
from statsmodels.formula.api import mixedlm

dataframe = pd.read_csv("pigdata.csv")

formula = "Weight ~ Time"

model = mixedlm(formula, dataframe, groups=dataframe["Pig"])
model = model.fit()

print(model.summary())

```

**Output**

```
         Mixed Linear Model Regression Results
========================================================
Model:            MixedLM Dependent Variable: Weight
No. Observations: 861     Method:             REML
No. Groups:       72      Scale:              11.3669
Min. group size:  11      Log-Likelihood:     -2404.7753
Max. group size:  12      Converged:          Yes
Mean group size:  12.0
--------------------------------------------------------
             Coef.  Std.Err.    z    P>|z| [0.025 0.975]
--------------------------------------------------------
Intercept    15.724    0.788  19.952 0.000 14.179 17.268
Time          6.943    0.033 207.939 0.000  6.877  7.008
Group Var    40.394    2.149
========================================================
```

**Interpreting Results**

The time was significant when modeling weight.

We can model weight as: 

`weight = 15.724 + Time*6.943`

### Ex. 2 Random Slope Model

In a random slope model we assume not only that the baseline for each subject is different, but when taking into consideration another independent variable, there is a difference in how the dependent variable responds.

Here we will reconsider the previous example. However in addition to the random intercept model, we will utilize a random slope model with respect to time. This is because different pigs have different baseline weights and may grow in weight differently over time. Additionally, we will see how the copper dose (Cu), affects our model.

**Code**

```python
import pandas as pd
import statsmodels.api as sm
from statsmodels.formula.api import mixedlm

dataframe = pd.read_csv("pigdata.csv")

# Note C() indicates a variable is categorical in nature
formula = "Weight ~ Time + C(Cu)"
random_coeff_covar = "~Time"

model = mixedlm(formula, 
                dataframe, 
                groups=dataframe["Pig"], 
                missing = 'none',
                re_formula=random_coeff_covar)

model = model.fit(method=["lbfgs"])

print(model.summary())
```

**Output**

```
           Mixed Linear Model Regression Results
===========================================================
Model:             MixedLM  Dependent Variable:  Weight
No. Observations:  861      Method:              REML
No. Groups:        72       Scale:               6.0374
Min. group size:   11       Log-Likelihood:      -2213.5346
Max. group size:   12       Converged:           Yes
Mean group size:   12.0
-----------------------------------------------------------
                 Coef.  Std.Err.   z    P>|z| [0.025 0.975]
-----------------------------------------------------------
Intercept        15.207    0.971 15.663 0.000 13.304 17.110
C(Cu)[T.2]       -0.145    1.347 -0.107 0.914 -2.785  2.496
C(Cu)[T.3]        1.746    1.358  1.286 0.199 -0.916  4.407
Time              6.939    0.080 86.923 0.000  6.783  7.095
Group Var        19.355    1.570
Group x Time Cov  0.267    0.153
Time Var          0.416    0.033
===========================================================
```

**Interpreting Results**

We can model weight as: 

`weight = 15.207 - (0.145 if Cu=2) + (1.746 if Cu = 3) + Time*6.939`

Only Time was significant when modeling weight, the other factor (copper dose) did not appear to be significant.

---


#### Sources


- https://www.statsmodels.org/stable/mixed_linear.html
- https://www.statsmodels.org/dev/examples/notebooks/generated/mixed_lm_example.html
- [Benevolent Stats](https://www.amazon.sg/Benevolent-Stats-MATLAB-Statistical-Analysis/dp/B08SGWNKZJ)