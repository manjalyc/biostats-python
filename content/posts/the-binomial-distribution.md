---
title: "The Binomial Distribution"
date: 2023-03-20
draft: false
author: "Cyriac Manjaly"
UseHugoToc: false
tags: ["Binomial Distribution", "Probability Mass Function", "Cumulative Density Function", "Code"]
showToc: true
weight: 10
cover:
    hidden: false
    relative: false
math: true
---

# The Binomial Distribution

The binomial distribution is the discrete probability distribution of the number of successes.

## [Criteria](https://openstax.org/books/introductory-statistics/pages/4-3-binomial-distribution)

1. Fixed Number of Trials. The number of trials in our model is **n**.

2. Each trial has only two possible outcomes. These are typically success, **p**, or failure, **q**.

3. Each trial is independent and identical. In other words the outcome of one trial does not affect the other.

## Examples


### Ex. 1: Probability of Flipping Exactly 8 Heads in 10 Tosses

Here we are interested in the [probability mass function](https://www.itl.nist.gov/div898/handbook/eda/section3/eda366i.htm). Using the scipy library, the probability mass function takes the form: [pmf(x, n, p)](https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.stats.binom.html). In our situation, x = 8, n = 10, and p = 0.5 (assuming a 50% chance of getting a head).

#### Code
```python
from scipy.stats import binom

prob_ex1 = binom.pmf(8, 10, 0.5)
print("Ex. 1 Probability:", prob_ex1)
```
#### Output

` Ex. 1 Probability: 0.04394531250000004`

So there is a 4.4% probability of flipping exactly 8 heads in 10 tosses.


### Ex. 2: Probability of Flipping Less than 6 Heads

Here we are interested in the [cumulative distribution function](https://www.radfordmathematics.com/probabilities-and-statistics/binomial-distribution/binomial-cumulative-distribution-function.html) which tells you the probability of observing less than or equal to k successes across n trials. Using the scipy library the cumulative distribution function takes the form: [cdf(k, n, p)](https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.stats.binom.html). In our situation n = 10 and p = 0.5. However, **k = 5**, not 6, as we want all trials less than or **equal to** k successes.

#### Code
```python
from scipy.stats import binom

prob_ex2 = binom.cdf(5, 10, 0.5)
print("Ex. 2 Probability:", prob_ex2)
```

#### Output
`Ex. 2 Probability: 0.623046875`

So there is a 62.3% probability of flipping less than 6 heads.


## Other Statistics

The mean, or expected value, of the binomial distribution = np

The variance of the binomial distribution = npq

#### Code
```python
from scipy.stats import binom

n, p = 10, 0.5
mean, variance = binom.stats(n, p)
print("Expected Number (mean) of heads across 10 flips: ", mean)
print("Variance of heads across 10 flips: ", variance)
```

#### Output
`Expected Number (mean) of heads across 10 flips:  5.0`

`Variance of heads across 10 flips:  2.5`


#### Sources

- [Benevolent Stats](https://www.amazon.sg/Benevolent-Stats-MATLAB-Statistical-Analysis/dp/B08SGWNKZJ)