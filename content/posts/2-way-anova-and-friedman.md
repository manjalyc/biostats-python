---
title: "2-way ANOVA, Repeated Measures ANOVA, and Friedman"
date: 2023-04-05
draft: false
author: "Cyriac Manjaly"
UseHugoToc: false
tags: ["multiple sample tests", "2-way anova", "friedman", "repeated measures", "Code"]
showToc: true
weight: 25
cover:
    hidden: false
    relative: false
math: true
---

# 2-way ANOVA, Repeated Measures ANOVA, and Friedman

Up to this point, we have been considering tests for 1 independent variable. The 2-way ANOVA lets us test against 2 independent variables and can be used as a repeated measures ANOVA. The Friedman Test is a non-parametric alternative to both of these!

## Examples

---

### Ex. 1: 2-way ANOVA

What if we want to see the impact of 2 factors,studying flashcards and/or taking notes, on test scores? We could run a 2-way ANOVA with an interaction term as follows:

**Data in '*2way-anova.csv*'**

|test_score|studied_flashcards|took_notes|
|----------|------------------|----------|
|90        |y                 |n         |
|97        |y                 |y         |
|67        |n                 |y         |
|92        |y                 |n         |
|63        |n                 |n         |
|98        |y                 |y         |
|45        |n                 |n         |
|99        |y                 |y         |
|86        |n                 |y         |
|87        |y                 |n         |

**Code**
```python
import pandas as pd
import statsmodels.api as sm
from statsmodels.formula.api import ols

dataframe = pd.read_csv("2way-anova.csv")

# Note the C indicates the data is categorical
formula = "test_score ~ C(studied_flashcards) + C(took_notes) + C(took_notes):C(studied_flashcards)"

model = ols(formula, data=dataframe).fit()

print(model.summary())
```

**Output**

*Note Output is Trimmed*
```
...
=================================================================================================================
                                                    coef    std err          t      P>|t|      [0.025      0.975]
-----------------------------------------------------------------------------------------------------------------
Intercept                                        54.0000      5.456      9.898      0.000      40.651      67.349
C(studied_flashcards)[T.y]                       35.6667      7.043      5.064      0.002      18.433      52.901
C(took_notes)[T.y]                               22.5000      7.715      2.916      0.027       3.621      41.379
C(took_notes)[T.y]:C(studied_flashcards)[T.y]   -14.1667      9.961     -1.422      0.205     -38.539      10.206
...
```

Note the P>|t| column indicates the p-values. At a significance level of 5%, studying flashcards and taking notes had a significant effect on test scores, but their interaction didn't!

We can model test scores as a function of studying flashcards and taking notes as:

test_score = 54 + (35.7 if studied flashcards) + (22.5 if took notes) - (14.2 if studied flashcards and took notes)

---

### Ex. 2: Repeated Measures ANOVA

What if we wanted to determine if the type of rim was significant in determining the cost of refitting the wheels of a car? We have 4 cars with the cost to refit each wheel per rim type. We can use a repeated measures ANOVA to determine if the rim_type is significant in determining the cost.


**Data in 'repeated-measures-anova.csv'**

|car_id|rim_type|cost|
|------|--------|----|
|1     |steel   |250 |
|1     |aluminum|300 |
|1     |chrome  |350 |
|2     |steel   |500 |
|2     |aluminum|500 |
|2     |chrome  |550 |
|3     |steel   |150 |
|3     |aluminum|225 |
|3     |chrome  |250 |
|4     |steel   |100 |
|4     |aluminum|125 |
|4     |chrome  |150 |

**Code**

```python
import pandas as pd
from statsmodels.stats.anova import AnovaRM

dataframe = pd.read_csv("repeated-measures-anova.csv")

model = AnovaRM(dataframe,depvar="cost",subject="car_id", within=["rim_type"])
print(model.fit())
```

**Output**

```
                Anova
=====================================
         F Value Num DF Den DF Pr > F
-------------------------------------
rim_type 16.2000 2.0000 6.0000 0.0038
=====================================
```

Hence we find that the rim_type is a significant factor in determining the cost to refit the wheels of a car with a p-value of 0.0038!

---


### Ex. 3: Friedman

Now what if we wanted to do a repeated measures anova for the above data, but it was non-parametric (not normally distributed)? We could use a friedman test!

**Data in '*friedman.csv*'**

|car_id|rim_type|cost|
|------|--------|----|
|1     |steel   |250 |
|1     |aluminum|300 |
|1     |chrome  |3500|
|2     |steel   |500 |
|2     |aluminum|500 |
|2     |chrome  |550 |
|3     |steel   |2000|
|3     |aluminum|2500|
|3     |chrome  |2500|
|4     |steel   |1000|
|4     |aluminum|1250|
|4     |chrome  |1500|


**Code**

```python
import pandas as pd
from pingouin import friedman

dataframe = pd.read_csv("friedman.csv")

stats = friedman(data=dataframe, dv="cost",within="rim_type", subject="car_id")
print(stats)
```

**Output**

```
            Source      W  ddof1    Q     p-unc
Friedman  rim_type  0.875      2  7.0  0.030197
```

So according to our friedman test, the rim_types are significant in determining costs!

#### Sources

- https://www.statsmodels.org/dev/_modules/statsmodels/stats/anova.html
- https://www.statsmodels.org/dev/generated/statsmodels.stats.anova.AnovaRM.html#statsmodels.stats.anova.AnovaRM-returns
- https://pingouin-stats.org/build/html/generated/pingouin.friedman.html
- [Benevolent Stats](https://www.amazon.sg/Benevolent-Stats-MATLAB-Statistical-Analysis/dp/B08SGWNKZJ)