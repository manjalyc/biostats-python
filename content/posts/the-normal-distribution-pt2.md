---
title: "The Normal Distribution (Pt. 2)"
date: 2023-04-01
draft: false
author: "Cyriac Manjaly"
UseHugoToc: false
tags: ["Normal Distribution", "Gaussian Distribution", "Anderson Darling Test", "Mean", "Standard Deviation", "Code"]
showToc: true
weight: 13
cover:
    hidden: false
    relative: false
math: true
---

# The Normal Distribution (Pt. 2)

The normal distribution is a continuous probability distribution that is symmetrical around its mean and designed for independent, random variables.

In this section, we will analyze how to work with normally distributed data. To calculate CDF and z-scores, please refer to part 1.

## Working with The Normal Distribution

---

### Anderson Darling Test - Test if data is normally distributed

The Anderson Darling Test lets us know if the data is normally distributed. The code below will break down the output by significance levels. Lets consider if the following test scores are normally distributed:

```python
from scipy.stats import anderson

test_scores = [95, 60, 80, 90, 76, 30, 75, 80, 83, 74]

res = anderson(test_scores)
adstat = res.statistic

print("Anderson Darling Statistic:", adstat)
for (cv, sl) in zip(res.critical_values, res.significance_level):
    if cv <= adstat:
        print(f"\tNormal at {sl}% significance. <= {cv}")
    else:
        print(f"\tNot Normal at {sl}% significance. > {cv}") 
```

Output

```
Anderson Darling Statistic: 0.7297082740659437
        Normal at 15.0% significance. >= 0.501
        Normal at 10.0% significance. >= 0.57
        Normal at 5.0% significance. >= 0.684
        Not Normal at 2.5% significance. < 0.798
        Not Normal at 1.0% significance. < 0.95
```
The value of the statistic exceeds the critical value associated with a significance level of 5.0%, so the null hypothesis may be rejected at a significance level of 5.0%, but not at a significance level of 2.5%. In other words at significance levels above 5.0% the data can be considered normal, but at levels below 2.5% the data cannot be considered normal.


---
### Get the mean and standard deviation of a data set.

To calculate the mean and standard deviation of the above test scores:

```Python
import numpy as np

test_scores = [95, 60, 80, 90, 76, 30, 75, 80, 83, 74]

print("Mean:", np.mean(test_scores))
print("Std. Deviation:", np.std(test_scores))
```

Output
```
Mean: 74.3
Std. Deviation: 17.280335644888382
```

#### Sources
- https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.norm.html
- https://statisticsbyjim.com/basics/normal-distribution/
- https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.anderson.html
- https://docs.python.org/3.8/library/statistics.html#statistics.NormalDist
- [Benevolent Stats](https://www.amazon.sg/Benevolent-Stats-MATLAB-Statistical-Analysis/dp/B08SGWNKZJ)