---
title: "Getting Started"
date: 2023-03-19T11:58:04-04:00
draft: false
author: "Cyriac Manjaly"
UseHugoToc: false
tags: ["Guide", "Setup"]
showToc: false
cover:
    hidden: false
    relative: false
weight: 1
---

This page is meant to a short guide on getting a suitable  environment setup. It is not meant to be a complete guide on installing python and popular components of the scientific computing stack, but rather point to resources that do a better job than I ever could.


## For Beginners

Anaconda is the world's most popular data science platform, and provides packages that make up the "SciPy" stack. The easiest way to install both python and the popular packages that make up the SciPy stack such as [pandas](https://pandas.pydata.org/), [NumPy](https://numpy.org/) and [Matplotlib](https://matplotlib.org/) is via Anaconda.

Anaconda can be installed [from their website](https://docs.continuum.io/anaconda/install)

Anaconda also [provides a getting started guide](https://docs.anaconda.com/anaconda/user-guide/getting-started/)

## For Intermediate Users

The examples utilized throughout this website depend on Python 3.6 or newer. Pip packages utilized by example code include: [pandas](https://pypi.org/project/pandas/), [numpy](https://pypi.org/project/numpy/), and  [matplotlib](https://pypi.org/project/matplotlib/).

### Recommended

If you are comfortable with python, you might also want to consider installing the ipdb package for a better interactive experience. See [the interactive mode section of our tips]({{< ref "tips#interactive-mode" >}})