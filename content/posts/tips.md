---
title: "Tips"
date: 2023-03-19T13:53:02-04:00
draft: false
author: "Cyriac Manjaly"
UseHugoToc: false
tags: ["Guide", "Tips"]
showToc: false
weight: 2
cover:
    hidden: false
    relative: false
---

This page is meant to give some more advanced tips and tricks for those already comfortable with python, or seeking to do a bit more.

## Interactive Mode

Python is an interpreted language. This means it runs code line-by-line. It also means that you can enter the "interpreter" at any point while a program is executing and view variables, mess with the data, etc.

At any point in your python script you can utilize the python debugger (pdb) via these two lines to enter an interactive mode:

```python
import pdb
pdb.set_trace()
```

In interactive mode you can see the values of variables, call functions, and do essentially anything that you could do in a python program.

Even better than pdb is ipdb, an enhanced version of pdb, which supports everything pdb does, but has color, has tab-enabled auto-complete, etc. However it has to be explicitly installed as it is not included by default in Anaconda or a standard python installation.

```python
import ipdb as pdb
pdb.set_trace()
```

____


