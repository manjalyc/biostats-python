---
title: "2-sample Tests"
date: 2023-04-03
draft: false
author: "Cyriac Manjaly"
UseHugoToc: false
tags: ["2-sample tests", "2-sample sign test for medians", "2-sample t-test", "welch test", "paired t-test", "wilcoxon sign rank", "wilcoxon rank sum", "Code"]
showToc: true
weight: 30
cover:
    hidden: false
    relative: false
math: true
---

# 2-Sample Tests

Two-sample tests are used to compare a sample to another sample. Here we provide examples for conducting a two-sample t-test, a paired t-test, two-sample Wilcoxon Rank Sum test, and two-sample Wilcoxon Sign Rank test. 

## Examples

---
### Ex. 1: Two-sample t-test

What if we want to compare the average length of songs in two different albums we could conduct a two sample t-test:

**Code**
```python
from scipy.stats import ttest_ind

album_1_song_lengths = [103, 83, 89, 97, 124, 187, 98, 158, 128]
album_2_song_lengths = [232, 243, 120, 188, 198, 183, 167, 170, 31]

print(ttest_ind(album_1_song_lengths, album_2_song_lengths))
```

**Output**

`Ttest_indResult(statistic=-2.1416068083809683, pvalue=0.047960304114417596)`

At a significance level of 5%, given our p-value of 0.048, we would find that our album song lengths significantly differed!

---

### Ex. 2: Two-Sample t-test with unequal variances (Welch Test)

A normal two-sample t-test assumes equal variances, but what if we didn't hold that assumption? We can conduct what is known as a Welch Test.

**Code**

```python
from scipy.stats import ttest_ind

a1_lengths = [103, 83, 89, 97, 124, 187, 98, 158, 128]
a2_lengths = [232, 243, 120, 188, 198, 183, 167, 170, 31]

print(ttest_ind(a1_lengths, a2_lengths, equal_var = False))
```

**Output**

`Ttest_indResult(statistic=-2.1416068083809683, pvalue=0.05275748318343372)`

At a significance level of 5%, given our p-value of 0.053, we would not find that our album song lengths significantly differed when we don't assume the variances were equal!

---

### Ex. 3: Wilcoxon Rank Sum Test

What if our data is not normally distributed and has huge outliers? We can conduct a Wilcoxon Rank Sum Test!

**Code**

```python
from scipy.stats import ranksums

a1_lengths = [103, 83, 89, 97, 124, 187, 98, 158, 128]
a2_lengths = [1232, 243, 120, 1188, 198, 183, 167, 170, 31]

print(ranksums(a1_lengths, a2_lengths))
```

**Output**

`RanksumsResult(statistic=-2.163402849872905, pvalue=0.030510208580440113)`

At a significance level of 5%, given our p-value of 0.031, we would find that our album song lengths significantly differed.

---

### Ex. 4: Paired T-Test

What if we want to compare two samples of data that are paired in some manner? For example, lets consider a group of people who take two versions of a test, and we want to see if the test scores significantly differed by version. We could do a paired t-test!

*Note that the data is paired, ie. the same individual who scored a 98 on version A scored an 87 on version B.*

**Code**

```python
from scipy.stats import ttest_rel

testA_scores = [98, 60, 80, 90, 76, 30, 75, 80, 83, 74]
testB_scores = [87, 63, 78, 72, 68, 30, 59, 76, 63, 78]

print(ttest_rel(testA_scores, testB_scores))
```

**Output**

`TtestResult(statistic=2.597324779316962, pvalue=0.028864598957001886, df=9) `

At a significance level of 5%, given our p-value of 0.029, we would find that the test scores between the two versions significantly differed!

---



### Ex. 5: Wilcoxon Sign Rank Test

Consider the paired t-test example above, but what if we have outliers, or the data is non-parametric (not normally distributed)? We can conduct a wilcoxon sign rank test!

*Note that the data is paired, ie. the same individual who scored a 98 on version A scored a 37 on version B.*

**Code**

```python
from scipy.stats import wilcoxon

testA_scores = [98, 60, 80, 90, 76, 30, 75, 80, 83, 74]
testB_scores = [37, 63, 78, 22, 28, 30, 59, 10, 63, 78]

print(wilcoxon(testA_scores, y=testB_scores))
```

**Output**

`WilcoxonResult(statistic=5.0, pvalue=0.038151710173415135)`

At a significance level of 5%, given our p-value of 0.038, we would find that the test scores between the two versions significantly differed!

#### Sources

- https://www.statsmodels.org/dev/generated/statsmodels.stats.descriptivestats.sign_test.html
- https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.ranksums.html
- https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.wilcoxon.html
- https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.ttest_rel.html
- https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.ttest_ind.html
- [Benevolent Stats](https://www.amazon.sg/Benevolent-Stats-MATLAB-Statistical-Analysis/dp/B08SGWNKZJ)