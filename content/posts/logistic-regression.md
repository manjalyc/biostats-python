---
title: "Logistic Regression"
date: 2023-04-15
draft: false
author: "Cyriac Manjaly"
UseHugoToc: false
tags: ["multiple sample tests", "Logistic Regression", "Code"]
showToc: true
weight: 50
cover:
    hidden: false
    relative: false
math: true
---

# Logistic Regression

What do we do if we want to model a dependent variable, but our dependent variable is not continuous but categorical? We can use a logistic regression!

# Example

What if we wanted to predict if someone studied with flashcards given their test score, whether or not they took notes, and the interaction of these two dependent variables? We could run a logistic regression as follows:

**Data in '*logistic.csv*'**

|test_score|studied_flashcards|took_notes|
|----------|------------------|----------|
|90        |y                 |n         |
|95        |y                 |y         |
|67        |n                 |y         |
|82        |y                 |n         |
|63        |n                 |n         |
|68        |y                 |y         |
|45        |n                 |n         |
|96        |y                 |y         |
|86        |n                 |y         |
|87        |y                 |n         |
|54        |y                 |n         |


**Code**

```python
import pandas as pd
import statsmodels.api as sm
from statsmodels.formula.api import glm

dataframe = pd.read_csv("logistic.csv")

# Note the C indicates the data is categorical
formula = "C(studied_flashcards) ~ C(took_notes) + test_score + C(took_notes)*test_score"

model = glm(formula, data=dataframe, family=sm.families.Binomial()).fit()

print(model.summary())
```

**Output**

*Note Output is Trimmed
```
...
                                    coef    std err          z      P>|z|      [0.025      0.975]
-------------------------------------------------------------------------------------------------
Intercept                         6.8680      5.644      1.217      0.224      -4.194      17.930
C(took_notes)[T.y]               -2.0024      8.550     -0.234      0.815     -18.760      14.756
test_score                       -0.1162      0.091     -1.275      0.202      -0.295       0.062
C(took_notes)[T.y]:test_score     0.0515      0.120      0.428      0.669      -0.185       0.288
...
```

**Interpreting Coefficients**

Unlike our other models, the coefficients function a little differently when we do a logistic regression. **First, compared to Matlab, these coefficients are negative as the fit works with the *negative* natural log.**

A coefficient of -2 indicates that the probability increased by e^2. Generally, a coefficient, c, indicates the odds ratio changed by e^-c .

For continuous variables, every one unit increase of the continuous variable results in the odds ratio being changed by e^-c times.


So what is the probability that someone who scored a 98 and took notes also studied flashcards?

```
-ln(odds) = 6.8680 - 2.0024 - 0.1162*98 + 0.0515*98
odds = 4.3710357729297580237
odds = p / (1-p)
4.3710 = p / (1-p)
p = .814
```

So there is a 81.4% probability that someone who scored a 98 and took notes also studied flashcards.

---

#### Sources

- https://en.wikipedia.org/wiki/Logistic_regression
- https://www.statsmodels.org/dev/examples/notebooks/generated/glm_formula.html
- https://www.statsmodels.org/stable/glm.html
- [Benevolent Stats](https://www.amazon.sg/Benevolent-Stats-MATLAB-Statistical-Analysis/dp/B08SGWNKZJ)