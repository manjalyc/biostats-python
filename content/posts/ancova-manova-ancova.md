---
title: "ANCOVA, MANOVA, & MANCOVA"
date: 2023-04-11
draft: false
author: "Cyriac Manjaly"
UseHugoToc: false
tags: ["multiple sample tests", "ANCOVA", "MANOVA", "MANCOVA", "Linear Regression", "Code"]
showToc: true
weight: 35
cover:
    hidden: false
    relative: false
math: true
---

# ANCOVA, MANOVA, & MANCOVA

We've already looked at a few variations of ANOVA for repeated measures, non-parametric data, etc.. Now we will consider (M)AN(C)OVA. The M simply means we have multiple dependent variables, and the the C simply means at least one of our independent variables is continuous.

## Examples

---

### Ex. 1: ANCOVA (& Linear Regression)

What if we want to see the impact of 2 factors,studying flashcards, and hours spent playing a video game (a continuous variable), on test scores? We could run an ANCOVA:

**Data in '*ancova.csv*'**

|test_score|studied_flashcards|hours_played|
|----------|------------------|-------------|
|90        |y                 |6.5          |
|97        |y                 |12           |
|67        |n                 |1            |
|92        |y                 |8            |
|63        |n                 |4            |
|98        |y                 |13.2         |
|45        |n                 |0            |
|99        |y                 |11.5         |
|86        |n                 |5            |
|87        |y                 |3            |

**Code**

```python
import pandas as pd
import statsmodels.api as sm
from statsmodels.formula.api import ols

dataframe = pd.read_csv("ancova.csv")

# Note the C indicates the data is categorical
formula = "test_score ~ C(studied_flashcards) + hours_played"

model = ols(formula, data=dataframe).fit()

print(model.summary())
```

**Output**

Note Output is Trimmed

```
...
==============================================================================================
                                 coef    std err          t      P>|t|      [0.025      0.975]
----------------------------------------------------------------------------------------------
Intercept                     91.5342      8.210     11.149      0.000      72.120     110.948
C(studied_flashcards)[T.y]    12.3449      6.452      1.913      0.097      -2.911      27.601
hours_played                  -2.7523      0.777     -3.543      0.009      -4.589      -0.915
...

```
So we find that at a significance level of 5% that studying flashcards was not a significant predictor (p=0.097), but hours_played was (p=0.009).

We can model our test scores as:

test_score = 91.5 + (12.3 if studied flashcards) - 2.8*(hours_played)

---

### Ex. 2: MANCOVA (and hence MANOVA)

We will now perform a MANCOVA, which also covers how to run a MANOVA. Lets modify our ANCOVA data to include two tests, testA and testB, whose scores we want to model. How does whether or not a student studied flashcards and the amount of hours they played affect their test scores?


**Data in '*mancova.csv*'**

|testA-scores|testB_scores|studied_flashcards|hours_played|
|------------|------------|------------------|------------|
|90          |80          |y                 |6.5         |
|97          |83          |y                 |0.5         |
|67          |90          |n                 |12          |
|92          |78          |y                 |4           |
|63          |74          |n                 |8           |
|98          |85          |y                 |2           |
|45          |87          |n                 |13.2        |
|99          |73          |y                 |1.5         |
|86          |99          |n                 |5           |
|87          |70          |y                 |7.4         |


**Code**

```python
import pandas as pd
from statsmodels.multivariate.manova import MANOVA

dataframe = pd.read_csv("manova.csv")

# Note the C indicates the data is categorical
formula = "testA_scores + testB_scores ~ C(studied_flashcards) + hours_played"

model = MANOVA.from_formula(formula, data=dataframe).mv_test()

print(model.summary())
```

**Output**

```
                 Multivariate linear model
============================================================

------------------------------------------------------------
       Intercept         Value  Num DF Den DF F Value Pr > F
------------------------------------------------------------
          Wilks' lambda  0.0409 2.0000 6.0000 70.3193 0.0001
         Pillai's trace  0.9591 2.0000 6.0000 70.3193 0.0001
 Hotelling-Lawley trace 23.4398 2.0000 6.0000 70.3193 0.0001
    Roy's greatest root 23.4398 2.0000 6.0000 70.3193 0.0001
------------------------------------------------------------

------------------------------------------------------------
  C(studied_flashcards)  Value  Num DF Den DF F Value Pr > F
------------------------------------------------------------
           Wilks' lambda 0.3491 2.0000 6.0000  5.5935 0.0425
          Pillai's trace 0.6509 2.0000 6.0000  5.5935 0.0425
  Hotelling-Lawley trace 1.8645 2.0000 6.0000  5.5935 0.0425
     Roy's greatest root 1.8645 2.0000 6.0000  5.5935 0.0425
------------------------------------------------------------

------------------------------------------------------------
       hours_played      Value  Num DF Den DF F Value Pr > F
------------------------------------------------------------
           Wilks' lambda 0.3517 2.0000 6.0000  5.5288 0.0435
          Pillai's trace 0.6483 2.0000 6.0000  5.5288 0.0435
  Hotelling-Lawley trace 1.8429 2.0000 6.0000  5.5288 0.0435
     Roy's greatest root 1.8429 2.0000 6.0000  5.5288 0.0435
============================================================
```

Note that if you are coming from R, the test-statistics reported come from Pillai's trace. In this case we find that studying flashcards (p=0.0425) and hours_played (p=0.0435) were both significant in determining test scores.

---

#### Sources

- https://www.statsmodels.org/stable/anova.html
- https://www.tutorialspoint.com/how-to-perform-an-ancova-in-python
- https://towardsdatascience.com/manova-97e675a96158
- [Benevolent Stats](https://www.amazon.sg/Benevolent-Stats-MATLAB-Statistical-Analysis/dp/B08SGWNKZJ)