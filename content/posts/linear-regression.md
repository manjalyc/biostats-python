---
title: "Linear Regression"
date: 2023-04-12
draft: false
author: "Cyriac Manjaly"
UseHugoToc: false
tags: ["multiple sample tests", "ANCOVA", "Linear Regression", "Code"]
showToc: true
weight: 45
cover:
    hidden: false
    relative: false
math: true
---

# Linear Regression

Note this page is verbatim from the ANCOVA example on the *ANCOVA, MANOVA, & MANCOVA* page.



# ANCOVA & Linear Regression Example

What if we want to see the impact of 2 factors,studying flashcards, and hours spent playing a video game (a continuous variable), on test scores? We could run an ANCOVA:

**Data in '*ancova.csv*'**

|test_score|studied_flashcards|hours_played|
|----------|------------------|-------------|
|90        |y                 |6.5          |
|97        |y                 |12           |
|67        |n                 |1            |
|92        |y                 |8            |
|63        |n                 |4            |
|98        |y                 |13.2         |
|45        |n                 |0            |
|99        |y                 |11.5         |
|86        |n                 |5            |
|87        |y                 |3            |

**Code**

```python
import pandas as pd
import statsmodels.api as sm
from statsmodels.formula.api import ols

dataframe = pd.read_csv("ancova.csv")

# Note the C indicates the data is categorical
formula = "test_score ~ C(studied_flashcards) + hours_played"

model = ols(formula, data=dataframe).fit()

print(model.summary())
```

**Output**

Note Output is Trimmed

```
...
==============================================================================================
                                 coef    std err          t      P>|t|      [0.025      0.975]
----------------------------------------------------------------------------------------------
Intercept                     91.5342      8.210     11.149      0.000      72.120     110.948
C(studied_flashcards)[T.y]    12.3449      6.452      1.913      0.097      -2.911      27.601
hours_played                  -2.7523      0.777     -3.543      0.009      -4.589      -0.915
...

```
So we find that at a significance level of 5% that studying flashcards was not a significant predictor (p=0.097), but hours_played was (p=0.009).

We can model our test scores as:

test_score = 91.5 + (12.3 if studied flashcards) - 2.8*(hours_played)

---

#### Sources

- https://www.tutorialspoint.com/how-to-perform-an-ancova-in-python
- [Benevolent Stats](https://www.amazon.sg/Benevolent-Stats-MATLAB-Statistical-Analysis/dp/B08SGWNKZJ)