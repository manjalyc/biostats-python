---
title: "The Poisson Distribution"
date: 2023-03-23
draft: false
author: "Cyriac Manjaly"
UseHugoToc: false
tags: ["Poisson Distribution", "Code"]
showToc: true
weight: 11
cover:
    hidden: false
    relative: false
math: true
---

# The Poisson Distribution

The poisson distribution is the discrete probability distribution of an **event happening a certain number of times (k)** within a given interval of time of space.

## [Criteria](https://openstax.org/books/introductory-statistics/pages/4-3-binomial-distribution)

1. You know the mean number, λ (lambda), of events  

2. Events are independent and identical. In other words the outcome of one event does not affect another event.

## Examples


### Ex. 1: Probability of hitting 10 free-throws when you average 8 a game.

#### Code
```python
from scipy.stats import poisson

prob_ex1 = poisson.pmf(10, 8)
print("Ex. 1 Probability:", prob_ex1)
```
#### Output

` Ex. 1 Probability: 0.09926153383153544`

So there is a 9.9% probability of hitting exactly 10 free-throws.

### Ex. 2: Probability of hitting less than 6 Free-Throws when you average 8 a game.


#### Code
```python
from scipy.stats import poisson

prob_ex2 = poisson.cdf(6, 8)
print("Ex. 2 Probability:", prob_ex2)
```

#### Output
`Ex. 2 Probability: 0.31337427753639774`

So there is a 31.3% probability of flipping less than 6 heads.

#### Sources

- https://www.scribbr.com/statistics/poisson-distribution/
- https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.poisson.html
- [Benevolent Stats](https://www.amazon.sg/Benevolent-Stats-MATLAB-Statistical-Analysis/dp/B08SGWNKZJ)