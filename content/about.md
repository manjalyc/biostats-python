---
title: "About"
layout: "about"
url: "/about/"
summary: about
---

This website is meant to be a companion guide to Vanderbilt University's *BSCI 3270: Statistical Methods in Biology* taught by Dr. Allison Leich-Hilbun with an focus on  MATLAB. The goal of this website is to expand the knowledge found in [Benevolent Stats: A MATLAB - Based Statistical Analysis Text](https://www.amazon.com/Benevolent-Stats-MATLAB-Statistical-Analysis/dp/B08SGWNKZJ) into the realm of python, another popular programming language for biostatistics researchers.

### Contact

Feel free to shoot me an email at cyriac.manjaly@vanderbilt.edu